//
//  ViewController.swift
//  IBTech Demo
//
//  Created by Emre on 6.07.2020.
//  Copyright © 2020 EmreSURK. All rights reserved.
//

import UIKit

class MoviesViewController: UIViewController {
    
    @IBOutlet var splashCoverView: UIView!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var searchBar: UISearchBar!
    
    var currentPage = 1
    var isLoading = false
    
    var movies = [MovieModal]() {
        didSet {
            filteredMovies = self.movies
        }
    }
    
    var filteredMovies = [MovieModal](){
        didSet {
            collectionView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.tidyUp(
            self,
            nibClass: MovieCollectionViewCell.className
        )
        
        collectionView.register(
            UINib(
                nibName: LoadMoreCollectionReusableView.className,
                bundle: nil
            ),
            forSupplementaryViewOfKind: LoadMoreCollectionReusableView.className,
            withReuseIdentifier: LoadMoreCollectionReusableView.className
        )
        
        /*
        collectionView.register(
            UINib(
                nibName: LoadMoreCollectionReusableView.className,
                bundle: nil
            ),
            forCellWithReuseIdentifier: LoadMoreCollectionReusableView.className
        )*/
        
        searchBar.delegate = self
        searchBar.returnKeyType = .done
        getMovies()
        navigationController?.setNavigationBarHidden(
            true,
            animated: false
        )
    }
    
    
    func getMovies()  {
        isLoading = true
        Services.getTopMovies(
            page: currentPage
        ) { [weak self] (movies, error) in
            self?.isLoading = false
            self?.splashCoverView.hideWithAlphaAnimation()
            
            guard let movies = movies?.results else {
                self?.handleError(error)
                return
            }
            
            self?.movies.append(
                contentsOf: movies
            )
        }
    }
    @objc func loadNextPage() {
        if false == isLoading {
            currentPage += 1;
            getMovies()
        }
    }
    
    
    func removeSearchFilter() {
        filter("")
    }
    
    func filter(_ text : String) {
        if text.isEmpty {
            filteredMovies = movies
            return
        }
        filteredMovies = movies.filter(with: text)
    }
    
    func openMovieDetails(
        _ movie : MovieModal
    ) {
        _ = MovieDetailViewController.push(
            self,
            movie: movie
        )
    }
    
}

extension MoviesViewController :
    UICollectionViewDataSource,
    UICollectionViewDelegate,
    UICollectionViewDelegateFlowLayout
{
    func collectionView(
        _ collectionView: UICollectionView,
        numberOfItemsInSection section: Int
    ) -> Int {
        return filteredMovies.count
    }
    
    func collectionView(
        _ collectionView: UICollectionView,
        cellForItemAt indexPath: IndexPath
    ) -> UICollectionViewCell {
        let cell : MovieCollectionViewCell = collectionView.cell(indexPath);
        
        let currentMovie = filteredMovies.get(indexPath.row)
        cell.fill(currentMovie)
        
        return cell
        
    }
    
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        insetForSectionAt section: Int
    ) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
    ) -> CGSize {
        let size:CGFloat = collectionView.frame.size.width / 2.0
        return CGSize(
            width: size,
            height: size * ( 3 / 2 ) + 30
        )
    }
    
    func scrollViewWillBeginDragging(
        _ scrollView: UIScrollView
    ) {
        searchBar.endEditing(true)
    }
    func collectionView(
        _ collectionView: UICollectionView,
        didSelectItemAt indexPath: IndexPath
    ) {
        collectionView.deselectItem(
            at: indexPath,
            animated: false
        )
        if let movie = filteredMovies.get(indexPath.row) {
            openMovieDetails(movie)
        }
    }
    
    func collectionView(
        _ collectionView: UICollectionView,
        viewForSupplementaryElementOfKind kind: String,
        at indexPath: IndexPath
    ) -> UICollectionReusableView {
        if UICollectionView.elementKindSectionFooter == kind {
            let footerView = collectionView.dequeueReusableSupplementaryView(
                ofKind: LoadMoreCollectionReusableView.className,
                withReuseIdentifier: LoadMoreCollectionReusableView.className,
                for: indexPath
                ) as! LoadMoreCollectionReusableView
            
            footerView.btnLoadMore.addTarget(
                self,
                action: #selector(loadNextPage),
                for: .touchUpInside
            )
            
            return footerView
        }
        
        return UICollectionReusableView()
    }
    
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        referenceSizeForFooterInSection section: Int
    ) -> CGSize {
        return CGSize(
            width: UIScreen.main.bounds.width,
            height: 70
        )
    }
    
}




// MARK: UISearchBarDelegate
extension MoviesViewController : UISearchBarDelegate {
    func searchBarCancelButtonClicked(
        _ searchBar: UISearchBar
    ) {
        view.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(
        _ searchBar: UISearchBar
    ) {
        searchBar.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard searchText.count > 2 else {
            removeSearchFilter()
            return
        }
        filter(searchText)
    }
}
