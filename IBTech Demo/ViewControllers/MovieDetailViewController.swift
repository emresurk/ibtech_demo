//
//  MovieDetailViewController.swift
//  IBTech Demo
//
//  Created by Emre on 8.07.2020.
//  Copyright © 2020 EmreSURK. All rights reserved.
//

import UIKit

class MovieDetailViewController: UIViewController {
    
    
    // TODO : Should we create a navigator layer?
    static func push(
        _ sender : UIViewController,
        movie : MovieModal
    ){
        let movieDetailViewController : MovieDetailViewController
            = UIStoryboard.initViewController()
        
        guard let navigationController = sender.navigationController else {
            let errMes = String(
                format: StaticStrings
                    .Errors
                    .DeveloperErrors
                    .noNavigationController,
                MovieDetailViewController.className
            )
            
            print(errMes)
            // TODO: Should we throw the error?
            return
        }
        movieDetailViewController.deleteBackButtonTitle()
        movieDetailViewController.movie = movie
        navigationController
            .pushViewController(
                movieDetailViewController,
                animated: true
        )
        
    }
    
    var movie : MovieModal! = nil
    
    @IBOutlet var movieImage: UIImageView!
    @IBOutlet var movieTitle: UILabel!
    @IBOutlet var movieInfoLabel: UILabel!
    @IBOutlet var avgPointLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    func updateUI() {
        navigationItem.title = movie.title
        movieImage.setImage(movie.posterPath)
        movieTitle.text = movie.title
        movieInfoLabel.text = movie.overview
        avgPointLabel.text = movie.avgPointText
    }
}
