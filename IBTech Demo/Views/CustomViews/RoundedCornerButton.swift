//
//  RoundedCornerButton.swift
//  IBTech Demo
//
//  Created by Emre on 8.07.2020.
//  Copyright © 2020 EmreSURK. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class RoundedCornerButton: UIButton {


    @IBInspectable var cornerRadius: Int = 5 
    
    override func draw(_ rect: CGRect) {
        makeRadius(cornerRadius)
    }

}
