//
//  LoadMoreCollectionReusableView.swift
//  IBTech Demo
//
//  Created by Emre on 14.07.2020.
//  Copyright © 2020 EmreSURK. All rights reserved.
//

import UIKit

class LoadMoreCollectionReusableView: UICollectionReusableView {

    @IBOutlet var btnLoadMore: RoundedCornerButton!
    
    
}
