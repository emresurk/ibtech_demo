//
//  MovieCollectionViewCell.swift
//  IBTech Demo
//
//  Created by Emre on 6.07.2020.
//  Copyright © 2020 EmreSURK. All rights reserved.
//

import UIKit

class MovieCollectionViewCell:
    UICollectionViewCell
{
    @IBOutlet private var movieImageView: UIImageView!
    @IBOutlet private var movieTitle: UILabel!
    
    func fill(
        _ movie : MovieModal?
    ) {
        
        guard let movie = movie else {
            movieImageView.image = nil
            movieTitle.text = nil
            return
        }
        
        movieTitle.text = movie.originalTitle
        movieImageView.setImage(movie.posterPath)
        
    }
}
