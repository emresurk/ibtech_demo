//
//  Data+Extensions.swift
//  IBTech Demo
//
//  Created by Emre on 6.07.2020.
//  Copyright © 2020 EmreSURK. All rights reserved.
//

import Foundation


extension Data {
    var toString : String {
        return String(data: self, encoding: .utf8) ?? ""
    }
}
