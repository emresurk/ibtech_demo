//
//  Dictionary+Extensions.swift
//  IBTech Demo
//
//  Created by Emre on 6.07.2020.
//  Copyright © 2020 EmreSURK. All rights reserved.
//

extension Dictionary {
    var queryString: String {
        if isEmpty {
            return ""
        }
        
        var output: String = "?"
        for (key,value) in self {
            output +=  "\(key)=\(value)&"
        }
        output = String(output.dropLast())
        return output
    }
}
