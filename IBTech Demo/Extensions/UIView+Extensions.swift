//
//  UIView+Extensions.swift
//  IBTech Demo
//
//  Created by Emre on 6.07.2020.
//  Copyright © 2020 EmreSURK. All rights reserved.
//

import Foundation
import UIKit


extension UIView {
    func hideWithAlphaAnimation(
        _ interval : Double = 0.3
    ){
        UIView.animate(
            withDuration: interval
        ) {
            self.alpha = 0
        }
    }
    func makeRadius(
        _ radius : Int = 12
    ) {
        self.layer.cornerRadius = CGFloat(radius)
        clipsToBounds = true
    }
}
