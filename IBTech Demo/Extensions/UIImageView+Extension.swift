//
//  UIImageView+Extension.swift
//  IBTech Demo
//
//  Created by Emre on 7.07.2020.
//  Copyright © 2020 EmreSURK. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {
    static let imageCache = NSCache<AnyObject, AnyObject>()

    
    func setImage(_ path : String? ) {
        
        image = nil
        
        guard var path = path else {
            return
        }
        
        if  !path.contains("http") &&
            !path.contains("https") {
            path = CoreServices.baseImageURL + path;
        }
        if let url = URL(string: path) {
            if let imageFromCache = UIImageView.imageCache.object(forKey: url as AnyObject) as? UIImage {
                self.image = imageFromCache
                return
            }
            URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in

                if error != nil {
                    print(error as Any)
                    return
                }

                DispatchQueue.main.async(execute: {

                    if  let unwrappedData = data,
                        let imageToCache = UIImage(data: unwrappedData) {

                        //if self.imageURL == url {
                        self.image = imageToCache
                        //}

                        UIImageView.imageCache.setObject(imageToCache, forKey: url as AnyObject)
                    }
                })
            }).resume()
        }
    }
}

