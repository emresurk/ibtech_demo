//
//  UIViewController+Extensions.swift
//  IBTech Demo
//
//  Created by Emre on 6.07.2020.
//  Copyright © 2020 EmreSURK. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func handleError(
        _ serviceError : ServiceErrorModal?
    ) {
        var serviceError = serviceError
        // kick to login, show simple message, etc..
        
        if serviceError == nil {
            serviceError = ServiceErrorModal.networkError()
        }
        
        _ = showSimpleMessage(
            nil,
            serviceError!.statusMessage
        )
    }
    
    func showSimpleMessage(
        _ title : String? = nil ,
        _ body : String? = nil,
        addDoneButton : Bool = true,
        done : (() -> Void)? = nil
    ) -> UIAlertController {
        
        let controller = UIAlertController(
            title: title,
            message: body,
            preferredStyle: .alert
        )
        if addDoneButton {
            controller.addAction(
                UIAlertAction(
                    title: StaticStrings.UIWords.ok ,
                    style: .default,
                    handler: { (alert) in
                        if done != nil {
                            done!()
                        }
                        controller.dismiss(
                            animated: true,
                            completion: nil
                        )
                }))
        }
        
        
        present(
            controller,
            animated: true,
            completion: nil
        )
        
        return controller
    }
    
    func deleteBackButtonTitle() {
        self.navigationController?
            .navigationBar
            .backItem?
            .title = ""
    }
    
}
