//
//  Array+Extensions.swift
//  IBTech Demo
//
//  Created by Emre on 6.07.2020.
//  Copyright © 2020 EmreSURK. All rights reserved.
//

import Foundation


extension Array {
    func get(_ index : Int) -> Element? {
        if index < 0 || index > count {
            return nil
        }
        return self[index]
    }
}


extension Array where Element == MovieModal {
    func filter(with title : String) -> Array<MovieModal> {
        return filter { (element) -> Bool in
            (element.originalTitle?.lowercased() ?? "").contains(title.lowercased())
        }
    }
}
