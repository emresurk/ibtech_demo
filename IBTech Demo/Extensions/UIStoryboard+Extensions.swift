//
//  UIStoryboard+Extensions.swift
//  IBTech Demo
//
//  Created by Emre on 8.07.2020.
//  Copyright © 2020 EmreSURK. All rights reserved.
//

import Foundation
import UIKit

extension UIStoryboard {
    static func initViewController<T:UIViewController>(
        storyboard : String = "Main",
        bundle : Bundle? = nil
    ) -> T{
        return UIStoryboard.init(
            name: storyboard,
            bundle: bundle
        ).instantiateViewController(
            withIdentifier: T.className
            ) as! T
    }
}
