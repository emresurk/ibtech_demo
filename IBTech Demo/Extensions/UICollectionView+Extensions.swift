//
//  UICollectionView+Extensions.swift
//  IBTech Demo
//
//  Created by Emre on 6.07.2020.
//  Copyright © 2020 EmreSURK. All rights reserved.
//

import Foundation
import UIKit



extension UICollectionView {
    
    func tidyUp(
        _ sender : UIViewController,
        nibClass : String?
    )  {
        if let nibClass = nibClass {
            register(
                UINib(nibName: nibClass, bundle: nil),
                forCellWithReuseIdentifier: nibClass
            )
        }
        delegate = sender as? UICollectionViewDelegate
        dataSource = sender as? UICollectionViewDataSource
    }
    
    func cell<T:UICollectionViewCell>(
        _ indexPath : IndexPath
    ) -> T {
        return dequeueReusableCell(
            withReuseIdentifier: T.className,
            for: indexPath
            ) as! T
    }
    
}



