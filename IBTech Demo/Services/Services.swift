//
//  GetTopMovies.swift
//  IBTech Demo
//
//  Created by Emre on 6.07.2020.
//  Copyright © 2020 EmreSURK. All rights reserved.
//

class Services {
    
    class func getTopMovies(
        page : Int,
        complated : @escaping CoreServices.apiResultFunction<MovieResultModal>
    ) {
        let params = ["page" : page ]
        
        CoreServices.get(
            api: CoreServices.ApiEndpoints.getTopMovies,
            parameters: params ,
            codableProtocol: MovieResultModal.self
        ) { (movies, error) in
            complated(movies,error)
        }
    }
}
