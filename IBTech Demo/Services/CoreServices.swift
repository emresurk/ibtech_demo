//
//  CoreServices.swift
//  IBTech Demo
//
//  Created by Emre on 6.07.2020.
//  Copyright © 2020 EmreSURK. All rights reserved.
//

import Foundation

class CoreServices {
    // MARK: Service URLs
    internal static let baseURL : String = "https://api.themoviedb.org/"
    internal static let baseImageURL : String = "https://image.tmdb.org/t/p/w500/"
    
    
    //MARK: API Endpoints.
    internal enum ApiEndpoints : String {
        case getTopMovies = "3/movie/top_rated"
    }
    
    internal typealias apiResultFunction<T:Codable> = (
       _ data : T?,
       _ error : ServiceErrorModal?
   )->()
    
    internal class func get<T: Codable>(
        api : ApiEndpoints,
        parameters : [String:Any],
        codableProtocol : T.Type ,
        completion : apiResultFunction<T>?
    ) {
        var parameters = parameters
        parameters["api_key"] = StaticStrings.APIKeys.themoviedbAPIKey
        parameters["language"] = "en-us"
        
        
        let urlString = baseURL + api.rawValue + parameters.queryString
        let url = URL(string: urlString)!

        let task = URLSession.shared.dataTask(
            with: url
        ) {(data, response, error) in
            DispatchQueue.main.async {
                guard let data = data else {
                    completion?(nil,ServiceErrorModal.networkError())
                    return
                }
                let decoder = JSONDecoder();
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                if let resultObject = try? decoder
                    .decode(
                        T.self,
                        from: data
                    ) {
                        completion?(resultObject,nil)
                        return
                }
                
                if  let errorObject = try? decoder
                    .decode(
                        ServiceErrorModal.self,
                        from: data
                    ) {
                    completion?(nil,errorObject)
                    return
                }
                
                completion?(nil,ServiceErrorModal.serverError())
            }
        }

        task.resume()
        
    }
}



