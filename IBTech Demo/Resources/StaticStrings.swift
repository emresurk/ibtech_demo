//
//  StaticStrings.swift
//  IBTech Demo
//
//  Created by Emre on 6.07.2020.
//  Copyright © 2020 EmreSURK. All rights reserved.
//

struct StaticStrings {
    struct UserMessages {
        static let noInternet = "İnternet erişiminiz mevcut değil."
        static let noServer = "Sunucumuz kısa süreliğinde bakımda, lütfen az sonra tekrar deneyiniz."
    }
    
    struct UIWords {
        static let ok = "Tamam"
        static let cancel = "Vazgeç"
    }
    
    struct APIKeys {
        static let themoviedbAPIKey : String = "11459cff1c1ce00e3202addab99f3a91"
    }
    
    struct Errors {
        struct DeveloperErrors {
            static let noNavigationController = "Error: Navigation controller is nil. You can not push %@."
        }
    }
}
