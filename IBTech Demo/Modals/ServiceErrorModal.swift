//
//  ServiceErrorModal.swift
//  IBTech Demo
//
//  Created by Emre on 6.07.2020.
//  Copyright © 2020 EmreSURK. All rights reserved.
//

import Foundation


class ServiceErrorModal: Codable {
     var statusCode: Int?
     var statusMessage: String?
     var success: Bool?

    init(_ statusMessage : String ) {
        self.statusMessage = statusMessage
    }
    
    class func networkError() -> ServiceErrorModal {
        return ServiceErrorModal(StaticStrings.UserMessages.noInternet)
    }
    
    class func serverError() -> ServiceErrorModal {
        return ServiceErrorModal(StaticStrings.UserMessages.noServer)
    }
    
}
