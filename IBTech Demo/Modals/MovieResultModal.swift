//
//  MovieResultModal.swift
//  IBTech Demo
//
//  Created by Emre on 6.07.2020.
//  Copyright © 2020 EmreSURK. All rights reserved.
//

import Foundation

class MovieResultModal: Codable {
    var page: Int?
    var totalResults: Int?
    var totalPages: Int?
    var results: [MovieModal]?

}
